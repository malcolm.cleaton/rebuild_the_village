// Game definition data.
var gd = {};
gd.jobs = {"chopping-wood": {}, "breaking-rocks": {}, "fishing": {}};
gd.people = {"bob": {}, "john": {}, "aaron": {}};

// Current game state data.
var g = {};

function startup() {
    // Save/load later, so just set up a default initial game state.
    g.resources = {};
    g.person_state = {bob: {skills: {}, job: 'chopping-wood'}, john: {job: 'none'}, "aaron": {job: 'none'}};
    g.job_state = {"chopping-wood": {}, "breaking-rocks": {}, "fishing": {}};

    initialise_view();
}

function initialise_view() {
    const m = document.getElementById('main');
    for (let [p_id, p_state] of Object.entries(g.person_state)) {
	m.innerHTML += make_person_home_div(p_id, gd.people[p_id], p_state);
    }
    for (let [j_id, j_state] of Object.entries(g.job_state)) {
	m.innerHTML += make_job_div(j_id, gd.jobs[j_id], j_state);
    }

    for (let [p_id, p_state] of Object.entries(g.person_state)) {
	place_person_img(p_id, gd.people[p_id], p_state);
    }
}

function make_person_home_div(p_id, p_data, p_state) {
    return `<div id="person-home-${p_id}" class="place" ondrop="drop(event)" ondragover="allowDrop(event)"></div>`;
}

function make_job_div(j_id, j_data, j_state) {
    return `<div id="job-${j_id}" class="place" ondrop="drop(event)" ondragover="allowDrop(event)"></div>`;
}

function place_person_img(p_id, p_data, p_state) {
    const html = `<img src="assets/${p_id}.jpg" draggable="true" ondragstart="drag(event)" id="person-img-${p_id}" width="88" height="31">`;
    let target_id = undefined;
    if (p_state.job == 'none') {
	target_id = `person-home-${p_id}`;
    } else {
	target_id = `job-${p_state.job}`;
    }
    const target = document.getElementById(target_id);
    target.innerHTML += html;
}


function person_id_from_person_img_id(person_img_id) {
    const pattern = /person-img-([a-z_-]+)/;
    const match = pattern.exec(person_img_id);
    return match[1];
}

/* Drag and drop functions */

function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev) {
    ev.preventDefault();
    const dragged_id = ev.dataTransfer.getData("text");
    const person_id = person_id_from_person_img_id(dragged_id);
    let job = 'none';
    const match = /job-([a-z_-]+)/.exec(ev.target.getAttribute('id'));
    if (match) {
	job = match[1];
    }
    g.person_state[person_id].job = job;
    ev.target.appendChild(document.getElementById(dragged_id));
}

